<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="{{mix('/css/app.css')}}">
</head>
<body>
<main>
    <div id="app">
        <div class="container">
            <h1>Главное окно</h1>
            <empsearch></empsearch>
            <empbuttons></empbuttons>
            <emptable></emptable>
        </div>
    </div>
</main>
<script src="{{mix('/js/app.js')}}"></script>
</body>
</html>
