<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class EmployeeResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'surname' => $this->surname,
            'birthday' => $this->birthday,
            'avatar' => $this->avatar->path,
            'position' => $this->position->name,
            'remote' => $this->remote,
            'city' => $this->city->name,
            'street' => $this->street->name,
            'house' => $this->house,
            'room' => $this->room,
        ];
    }
}
