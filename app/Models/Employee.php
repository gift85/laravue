<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    public function avatar()
    {
        return $this->belongsTo('App\Models\Avatar');
    }

    public function position()
    {
        return $this->belongsTo('App\Models\Position');
    }

    public function city()
    {
        return $this->belongsTo('App\Models\City');
    }

    public function street()
    {
        return $this->belongsTo('App\Models\Street');
    }
}
