<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use \Carbon\Carbon;

class PositionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('positions')->insert([
            [
                'name' => 'техник',
                "created_at" => Carbon::now(),
                "updated_at" => Carbon::now(),
            ],
            [
                'name' => 'программист',
                "created_at" => Carbon::now(),
                "updated_at" => Carbon::now(),
            ],
            [
                'name' => 'бухгалтер',
                "created_at" => Carbon::now(),
                "updated_at" => Carbon::now(),
            ],
            [
                'name' => 'директор',
                "created_at" => Carbon::now(),
                "updated_at" => Carbon::now(),
            ],
            [
                'name' => 'бэтмен',
                "created_at" => Carbon::now(),
                "updated_at" => Carbon::now(),
            ]
        ]);
    }
}
