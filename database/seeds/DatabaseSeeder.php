<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         factory(App\Models\Avatar::class, 25)->create();
         factory(App\Models\City::class, 25)->create();
         factory(App\Models\Street::class, 25)->create();
         $this->call(PositionsTableSeeder::class);
         factory(App\Models\Employee::class, 25)->create();
    }
}
