<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFkToEmployeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('employees', function (Blueprint $table) {
            $table->foreign('avatar_id')->references('id')->on('avatars');
            $table->foreign('position_id')->references('id')->on('positions');
            $table->foreign('city_id')->references('id')->on('cities');
            $table->foreign('street_id')->references('id')->on('streets');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('employees', function (Blueprint $table) {
            $table->dropForeign('avatar_id');
            $table->dropForeign('position_id');
            $table->dropForeign('city_id');
            $table->dropForeign('street_id');
        });
    }
}
