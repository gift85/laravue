<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\Models\Employee::class, function (Faker $faker) {
    return [
        'name' => $faker->firstName('male'),
        'surname' => $faker->lastName,
        'avatar_id' => App\Models\Avatar::all()->random()->id,
        'birthday' => $faker->dateTimeBetween('-40years', '-16years')->format('Y-m-d'),
        'position_id' => App\Models\Position::all()->random()->id,
        'remote' => (bool) $faker->numberBetween(0, 1),
        'city_id' => App\Models\City::all()->random()->id,
        'street_id' => App\Models\Street::all()->random()->id,
        'house' => $faker->numberBetween(1, 50),
        'room' => $faker->numberBetween(1, 100),
    ];
});
